# Master's Thesis

## Supporting Localised Democracy

**Experimenting with QR Codes and Smartphones
for Situated Engagement
in Social Housing Democracy**

This repository contains the LaTeX source for my Master's thesis. For compiled
PDFs, see Downloads.

The repository is available under the MIT license.
